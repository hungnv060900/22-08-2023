package models;

public class Triangle extends Shape {
    private int base;
    private int height;

    public Triangle(int base, int height) {
        this.base = base;
        this.height = height;
    }
    public Triangle(int base, int height,String color) {
        this.base = base;
        this.height = height;
        this.setColor(color);
    }

    public int getBase() {
        return base;
    }

    public void setBase(int base) {
        this.base = base;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public double getArea() {
        return base*height;
    }
    @Override
    public String toString() {
        return "Triangle [base=" + base + ", height=" + height +", color= " + this.getColor() + "]";
    }
    

}
