package models;

public class Retangle extends Shape {
    private int length;
    private int width;

    public Retangle() {
    }

    public Retangle(int length, int width) {
        this.length = length;
        this.width = width;
    }

    public Retangle(int length, int width, String color) {
        this.length = length;
        this.width = width;
        this.setColor(color);
    }

    public double getArea() {
        return this.length * this.width;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    @Override
    public String toString() {
        return "Retangle [length=" + length + ", width=" + width + ", color= " + this.getColor() + "]";
    }

}
