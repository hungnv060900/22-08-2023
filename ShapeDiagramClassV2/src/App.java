import models.Circle;
import models.Rectangle;
import models.Square;

public class App {
    public static void main(String[] args) throws Exception {
        //System.out.println("Hello, World!");
        Rectangle rectangle = new Rectangle("Yellow", false, 10, 20);
        System.out.println("Rectangle: ");
        System.out.println(rectangle.toString());
        System.out.println("Area: ");
        System.out.println(rectangle.getArea());
        System.out.println("Perimeter: ");
        System.out.println(rectangle.getPerimeter());

        Circle circle = new Circle("Blue", false, 5);
        System.out.println("Circle: ");
        System.out.println(circle.toString());
        System.out.println("Area: ");
        System.out.println(circle.getArea());
        System.out.println("Perimeter: ");
        System.out.println(circle.getPerimeter());

        Square square = new Square("Pink", false, 5, 5);
        System.out.println("Square: ");
        System.out.println(square.toString());
        System.out.println("Area: ");
        System.out.println(square.getArea());
        System.out.println("Perimeter: ");
        System.out.println(square.getPerimeter());
    }
}
