import models.MovableCircle;

public class App {
    public static void main(String[] args) throws Exception {
        //System.out.println("Hello, World!");
        MovableCircle movableCircle = new MovableCircle(4, 5, 4, 5, 2);
        System.out.println("Circle: ");
        System.out.println(movableCircle.toString());
        movableCircle.moveDown();
        movableCircle.moveDown();
        movableCircle.moveLeft();
        movableCircle.moveLeft();
        System.out.println(movableCircle.toString());
    }
}
