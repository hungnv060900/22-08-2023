import models.Retangle;
import models.Triangle;

public class App {
    public static void main(String[] args) throws Exception {
       // System.out.println("Hello, World!");
       Retangle retangle1 = new Retangle(10, 20, "Red");
       Retangle retangle2 = new Retangle(30, 40, "Blue");
       System.out.println("Retangle 1:");
       System.out.println(retangle1.getArea());
       System.out.println(retangle1.toString());
       System.out.println("Retangle 2:");
       System.out.println(retangle2.getArea());
       System.out.println(retangle2.toString());

       Triangle triangle1 = new Triangle(5, 5, "Green");
       Triangle triangle2 = new Triangle(10, 10, "Yellow");
       System.out.println("Triangle 1:");
       System.out.println(triangle1.getArea());
       System.out.println(triangle1.toString());
    System.out.println("Triangle 2:");
       System.out.println(triangle2.getArea());
       System.out.println(triangle2.toString());
    }
}
