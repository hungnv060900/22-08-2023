package models;

public class Square extends Rectangle {

    private double side; // Declare the side field

    public Square(double width, double length) {
        super(width, length);
        this.side = width; // or length, as both are the same for a square
    }

    public Square(double side) {
        super(side, side);
        this.side = side;
    }

    public Square(String color, boolean filled, double width, double length) {
        super(color, filled, width, length);
        this.side = width; // or length, as both are the same for a square
    }

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }

    public String toString() {
         return "Square [Rectangle[Shape[color = " + color + ", filled = " + filled + "], width = " + width + ", length ="+length+"]"+"]";
    }
    
}
