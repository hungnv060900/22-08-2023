import models.Circle;
import models.Retangle;

public class App {
    public static void main(String[] args) throws Exception {
        //System.out.println("Hello, World!");
        Circle circle = new Circle(3);
        System.out.println("Circle: ");
        System.out.println(circle.toString());
        System.out.println("Area :");
        System.out.println(circle.getArea());
        System.out.println("Perimeter: ");
        System.out.println(circle.getPerimeter());

        Retangle retangle = new Retangle(5, 7);
        System.out.println("Retangle: ");
        System.out.println(retangle.toString());
        System.out.println("Area :");
        System.out.println(retangle.getArea());
        System.out.println("Perimeter: ");
        System.out.println(retangle.getPerimeter());


    }
}
