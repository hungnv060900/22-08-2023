package models;

import interfaces.GeometricObject;

public class Retangle implements GeometricObject {
    private double width;
    private double length;
    public Retangle(double width, double length) {
        this.width = width;
        this.length = length;
    }
    @Override
    public String toString() {
        return "Retangle [width=" + width + ", length=" + length + "]";
    }
    @Override
    public double getArea() {
        return width*length;
    }
    @Override
    public double getPerimeter() {
        return (width+length)*2;
    }
    
}
